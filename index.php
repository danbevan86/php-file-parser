<?php

    function outputFiles($path){
        // Check directory exists or not
        if(file_exists($path) && is_dir($path)){
            // Search the files in this directory
            $files = glob($path ."/*");
            if(count($files) > 0){ // if there are files
                foreach($files as $file){
                    if(is_file("$file")){ // if its a file
                        $file_part = pathinfo($file);
                        if($file_part['extension'] !== "ini") { // don't parse ini files
                            $handle = fopen($file, "r");
                            $lines = []; // new lines array

                            // set column headers
                            $header[0] = 'ID';
                            $header[1] = 'appCode';
                            $header[2] = 'deviceId';
                            $header[3] = 'contactable';
                            $header[4] = 'subscription_status';
                            $header[5] = 'has_downloaded_free_product_status';
                            $header[6] = 'has_downloaded_iap_product_status';

                            $row = 0; // set the first row
                            $skip_row_number = array("1"); // skip over line 1

                            if (($handle = fopen($file, "r")) !== FALSE) {
                                while (($data = fgetcsv($handle, 5000, "\n")) !== FALSE) {
                                    $row++;	
                                    if (in_array($row, $skip_row_number)) { // if line 1
                                        continue;
                                    } else {
                                        $lines[] = $data; 
                                    }
                                }
                                fclose($handle);
                            }
                            $fp = fopen('file.csv', 'w');
                            fputcsv($fp, $header);
                            foreach ($lines as $line) {
                                fputcsv($fp, $line);
                            }
                            fclose($fp);
                            header("Content-type: text/csv");
                            header("Content-disposition: attachment; filename = file.csv");
                            readfile("file.csv");
                        }

                    } else if(is_dir("$file")){
                        outputFiles("$file");
                    }
                }
            } else{
                echo "ERROR: No such file found in the directory.";
            }
        } else {
            echo "ERROR: The directory does not exist.";
        }
    }

    outputFiles("parser_test");
?>